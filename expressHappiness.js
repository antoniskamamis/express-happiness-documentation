var restConf = {
    routes:[]
}
var fs = require('fs');
var colors = require('colors');
var flatSignature = [];
var app, router, confObj, kpis, rcv;
var path = require('path');

var eh = function(p_app, p_router, conf){
  /*
    local reference of the express application variable
   */
  app = p_app;

  /*
    local reference of the express router
   */
  router = p_router;

  /*
    get a hold of the directory from which the app was instansiated
   */
  var appDir = path.dirname(require.main.filename);

  /*
    set generate function which is called from the user to instansiate the
    expresshappiness application to successGenerate
   */ 
  this.generate = successGenerate;

  /*
    set default configuration object which can be overwritten from the user passed
    options
   */
  confObj = {

    /*
      the mock functionality
     */
    mockData:{
      enable: true,

      /*
        the folder where the mocks are located
       */
      folder: appDir + '/mockJSONs',
      global:false
    },

    /*
      the fields file where reusable fields are declared
     */
    reusableFieldsFile: appDir + '/expressHappiness/reusableFields.js',

    /*
      the error logging file where the error logging if enabled is written
     */
    errorFile:appDir + '/expressHappiness/errors.log',

    /*
      the user defined error handling objects
     */
    errorsConfigurationFile:appDir + '/expressHappiness/conf/errors.js',

    /*
      the routes defined from the user
     */
    apiConfigurationFile:appDir + '/expressHappiness/conf/restConf.js',

    /*
      the controller middleware functions declarations
     */
    controllersFile:appDir + '/expressHappiness/controllerFunctions.js'
  };

  if(!conf){
    conf = {};
  }

  /*
    overwrite the default configuration with the user provided options
   */
  for(var propertyName in conf) {
    if(conf.hasOwnProperty(propertyName)){
      confObj[propertyName] = conf[propertyName];
    }
  }

  // Keep it dry when checking for the files
  // Check that all needed files (all for now) are present and readable
  // TODO: change this to use fs.stat and the async module
  var errorOccurred = false;
  if(!fs.existsSync(confObj.reusableFieldsFile)){
    errorOccurred = true;
    var msg = "Reusable fields file (" + confObj.reusableFieldsFile + ") does not exist. Please create it according the documentation";
    console.log(msg.red.bgWhite);
  }
  if(!fs.existsSync(confObj.errorFile)){
    errorOccurred = true;
    var msg = "Error log file (" + confObj.errorFile + ") does not exist. Please create it according the documentation";
    console.log(msg.red.bgWhite);
  }
  if(!fs.existsSync(confObj.errorsConfigurationFile)){
    errorOccurred = true;
    var msg = "Errors configuration file (" + confObj.errorsConfigurationFile + ") does not exist. Please create it according the documentation"
    console.log(msg.red.bgWhite);
  }
  if(!fs.existsSync(confObj.apiConfigurationFile)){
    errorOccurred = true;
    var msg = "REST API configuration file (" + confObj.apiConfigurationFile + ") does not exist. Please create it according the documentation"
    console.log(msg.red.bgWhite);
  }
  if(!fs.existsSync(confObj.controllersFile)){
    errorOccurred = true;
    var msg = "Controller functions definition file (" + confObj.controllersFile + ") does not exist. Please create it according the documentation"
    console.log(msg.red.bgWhite);
  }
  if(confObj.mockData.enable && !fs.existsSync(confObj.mockData.folder)){
    errorOccurred = true;
    var msg = "Mock data folder (" + confObj.mockData.folder + ") does not exist. Please create it according the documentation"
    console.log(msg.red.bgWhite);
  }

  if(errorOccurred){
    this.generate = failureGenerate;
  } else {

    /*
      get a reference of fileds loader module
     */
    var FL = require('./fieldsLoader.js');

    //get an instance of the fields loader
    var fieldsLoader = new FL(confObj.reusableFieldsFile);

    //get the defined routes
    restConf = require(confObj.apiConfigurationFile).conf(fieldsLoader);

    //set the defined routes in the global scope (remove)
    global.expressHappiness = {
      confObj: confObj
    };

    // get a reference of the parameters validation module
    rcv = require('./RESTcallsValidator.js');

    // instansiates the errors handling
    // and the reusable fields
    rcv.init(confObj);
    
    // asign the routes declarations
    rcv.assignConf(restConf);
    
    // get the controller middleware definitions
    kpis = require(confObj.controllersFile);
  }

  //not used, to be removed
  this.start = function(port){
    if(!port){
        var msg = "Please enter the port number for the app to run. Exiting.";
        console.log(msg.red.bgWhite);
        process.exit(1);
    } else {
        isPortTaken(port, function(err, taken){
            if(taken) {
                var msg = 'Port ' + port + ' is been used by another application. Exiting.';
                console.log(msg.red.bgWhite);
                process.exit(1);
            } else {
                p_app.listen(port);
                var msg = 'The application is listening on port ' + port + ' for connections.';
                console.log(msg.green);
            }
        });
    }
  }

}

/*
  checks if the port is taken
  UNUSED
*/
var isPortTaken = function(port, fn) {
    var net = require('net')
    var tester = net.createServer()
        .once('error', function (err) {
            if (err.code != 'EADDRINUSE') return fn(err)
            fn(null, true)
        })
        .once('listening', function() {
            tester.once('close', function() { fn(null, false) })
                .close()
        })
        .listen(port)
}


/* Reads the restConf and generates all available routes according to it.
 Also, it acts as debugger. For each of the generated routes the generate function checks if there is
 a corresponding method defined on the kpi controller, if there is mock data file available and it logs everything
 to the console.
 Missing methods are replaced by a generic one which handles their absence.
 Also, routes that do not provide mock data are configured so noMockAvailable function will be called whenever
 mock operation is on for them. For routes that actually have corresponding mock data files available the
 makeMockQuery function will be invoked for mock operation cases. Also, for efficiency reasons the makeMockQuery function
 for each of the routes that actually provide mock data is been generated once here (by reading the corresponding file)
 and it's been passed, so only one file read occurs for each route, only once, only at the kick off.
 It takes three parameters:
 @ router [object]: express framework router object
 @ baseUrl [string]: the base url of the routes to create
 @ preValidationMiddlewares [object][array]: holds all the middlewares that should be applied to the specific routes. These middlewares are been applied before validation
 it is an object. On restConf.js file each route might belong to one or more groups. For each group there might be different middlewares applied.
 This is achieved by using the group names as the first level keys of the array. Each key will hold another array which actual holds
 all the middlewares to be applied to the specific groups. Example:
 preValidationMiddlewares = {
   'userAccess' : [middlewareName1, middleware2],
   'adminAccess' : [middlewareName3, middleware4]
 }

 if we want to apply a middleware to all of the routes, no matter what then we can define this by including it on the key "eh-allRoutes".
 Example:
 preValidationMiddlewares = [
   'groupNameOne':[middlewareName1, middleware2],
   'groupNameTwo':[middlewareName3, middleware4],
   'eh-allRoutes':[middlewareToBeAppliedToAllRoutes1, middlewareToBeAppliedToAllRoutes2]
 ]
 @ postValidationMiddlewares [object][array]: holds all the middlewares that should be applied after the validation process and before the controller function invocation
 it is an object. On restConf.js file each route might belong to one or more groups. For each group there might be different middlewares applied.
 This is achieved by using the group names as the first level keys of the array. Each key will hold another array which actual holds
 all the middlewares to be applied to the specific groups. Example:
 postValidationMiddlewares = [
   'groupNameOne':[middlewareName1, middleware2],
   'groupNameTwo':[middlewareName3, middleware4]
 ]

 if we want to apply a middleware to all of the routes, no matter what then we can define this by including it on the key "eh-allRoutes".
 Example:
 postValidationMiddlewares = [
   'groupNameOne':[middlewareName1, middleware2],
   'groupNameTwo':[middlewareName3, middleware4],
   'eh-allRoutes':[middlewareToBeAppliedToAllRoutes1, middlewareToBeAppliedToAllRoutes2]
 ]
 */
var successGenerate = function(baseUrl, preValidationMiddlewares, postValidationMiddlewares){
    // create a new Express Router object
    var theRoute = router.route(baseUrl + '/');

    // puts an expresshappiness object to the route
    // with a getter and setter for the field
    // if the request is a GET then the field is looked in the req.query params
    // if it is anything else is looked in the req.body
    theRoute.get(putMethodsOnReq('get'));

    // in the previously created expressHappiness object adds
    // if there are any mocks
    // the apiPath
    // the routeAlias
    theRoute.get(putApipathToReq([], ''));

    // for the base path the isRoot variable is set to true in the req object
    theRoute.get(function(req, res, next){
        req.isRoot = true;
        return next();
    });

    // the base path renders the documentation overview as generated from 
    // express Happiness
    theRoute.get(function(req, res){
        var fs = require('fs');
        fs.readFile(__dirname + '/views/index.html', { 'encoding':'utf8'}, function(err, data){
            if(err){
                console.log(err);
            } else {
                var _ = require('underscore');
                var template = _.template(data);
                res.send(template({signature:flatSignature}));
            }
        });
    });

    // preinstansiate the routeObject with subRoutes the actual routes so the recursion can start
    var routeObject = {
        subRoutes:  restConf.routes
    };

    // generate the routes as defined in the user routes configuration file
    generateNodeRoutes(routeObject, '', [], router, baseUrl, preValidationMiddlewares, postValidationMiddlewares, []);

    // register errors as defined in the user defined errors file
    registerErrors(app, confObj.errorsConfigurationFile, confObj.errorFile);
};

var failureGenerate = function(){
    console.log("Routes generation process skipped due to errors. Please fix the errors accordingly and retry".red.bgWhite);
    process.exit(0);
}

/**
 * The module exports the express happiness function named eh(?)
 * @type {function}
 */
module.exports = eh;

/**
 * Generates and registers the routes for the application as defined from the user
 * @param  {Object} node                      The routes object currently being registered
 * @param  {String} nodeName                  The name of the current node
 * @param  {Array} path                       Array containing all paths till now
 * @param  {Express Router} router            The express js router
 * @param  {String} baseUrl                   The url in the current iteration where middlewares to be applied
 * @param  {Object} preValidationMiddlewares  Object with pre validation middlewares
 * @param  {Object} postValidationMiddlewares Object with post validation middlewares
 * @param  {Array} inheritedGroups            The groups from the route parents
 */
var generateNodeRoutes = function(node, nodeName, path, router, baseUrl, preValidationMiddlewares, postValidationMiddlewares, inheritedGroups){

    // if the current node has its own groups property the inherited groups array is
    // set to it replacing the parents groups
    if(node.hasOwnProperty('groups')){
        inheritedGroups = node.groups;
    }
  
    // The accepted HTTP methods
    var supportedTypes = ['get', 'post', 'put', 'delete'];

    // for all the supported HTTP methods
    for(var k=0; k<supportedTypes.length; k++){

        // if the current node has the the currently iterated HTTP method defined
        if(node.hasOwnProperty(supportedTypes[k])){

            // if the current node has its own groups replace the inherited Groups
            if(node[supportedTypes[k]].hasOwnProperty('groups')){
                inheritedGroups = node[supportedTypes[k]].groups;
            }

            // populate the flatSignature with the current method name
            // the method object from the node
            // and the pathe to this point
            flatSignature.push({
                type: supportedTypes[k],
                node:node[supportedTypes[k]],
                path:baseUrl + path.join('/') + '/' + nodeName
            });

            // create the mock middleware if available
            var mockQueryMiddleware = mockQueryGenerationMiddleWare(supportedTypes[k], path, nodeName, node[supportedTypes[k]].alias);

            // create a new express router object with the current route
            // to be attached to the express app at the end of this iterration
            var theRoute = router.route(baseUrl + path.join('/') + '/' + nodeName);

            // for the newly created route put the method urlalias mockdata info 
            // to the request expressHappiness object
            theRoute[supportedTypes[k]](putMethodsOnReq(supportedTypes[k]));
            theRoute[supportedTypes[k]](putApipathToReq(path, nodeName,
                node[supportedTypes[k]].alias, node[supportedTypes[k]].mock));

            // if there is a prevalidation middleware group entry for all routes
            // denoted with eh-allRoutes register all its middlewares for the current 
            // router
            if(preValidationMiddlewares.hasOwnProperty('eh-allRoutes')){
                for(var ii=0; ii<preValidationMiddlewares['eh-allRoutes'].length; ii++){
                    theRoute[supportedTypes[k]](preValidationMiddlewares['eh-allRoutes'][ii]);
                }
            }

            // for all the inherited groups and for all their middleware
            // add them to the current router
            for(var i=0; i<inheritedGroups.length; i++){
                if(preValidationMiddlewares.hasOwnProperty(inheritedGroups[i])){
                    for(var ii=0; ii<preValidationMiddlewares[inheritedGroups[i]].length; ii++){
                        theRoute[supportedTypes[k]](preValidationMiddlewares[inheritedGroups[i]][ii]);
                    }
                }
            }

            // add the attributes validation middleware
            theRoute[supportedTypes[k]](rcv.validateAttrs);

            // add the mockquery middleware function
            theRoute[supportedTypes[k]](mockQueryMiddleware);

            // add the mockmiddlewareapplied middleware which
            // if the mocking is enabled and if the route has mock data and they are 
            // attached to the request object they are returned here without
            // running the actual defined controller middleware for the route
            theRoute[supportedTypes[k]](mockMiddlewareApplied);

            // if there are post validation middlewares defined
            if(postValidationMiddlewares){
                // if there are middlewares for all routs in the post validation object
                if(postValidationMiddlewares.hasOwnProperty('eh-allRoutes')){
                    // register them in the current route
                    for(var ii=0; ii<postValidationMiddlewares['eh-allRoutes'].length; ii++){
                        theRoute[supportedTypes[k]](postValidationMiddlewares['eh-allRoutes'][ii]);
                    }
                }
                // for all the inherited groups and for all their middlewares
                // register them in the current router object
                for(var i=0; i<inheritedGroups.length; i++){
                    if(postValidationMiddlewares.hasOwnProperty(inheritedGroups[i])){
                        for(var ii=0; ii<postValidationMiddlewares[inheritedGroups[i]].length; ii++){
                            theRoute[supportedTypes[k]](postValidationMiddlewares[inheritedGroups[i]][ii]);
                        }
                    }
                }
            }

            var aliasedFunction = false;

            // if the node in question has an alias property
            if(node[supportedTypes[k]].alias){
                // if there is an controller middleware defined with a key matching the
                // controller function will be registered as a router middleware
                if(kpis.functions[node[supportedTypes[k]].alias] != undefined && kpis.functions[node[supportedTypes[k]].alias] != null){
                    theRoute[supportedTypes[k]](kpis.functions[node[supportedTypes[k]].alias]);
                    aliasedFunction = true;
                }
            }

            // if there is no aliased middleware function defined
            if(!aliasedFunction){
                // if there is a function defined in the form of 
                // <http method>:path.join('/')/nodename
                // register it as a middleware
                if(kpis.functions[supportedTypes[k] + ":" + path.join('/') + '/' + nodeName] != undefined && kpis.functions[supportedTypes[k] + ":" + path.join('/') + '/' + nodeName] != null){
                    theRoute[supportedTypes[k]](kpis.functions[supportedTypes[k] + ":" + path.join('/') + '/' + nodeName]);
                } else {
                  // if there is no controller function found register a generic callback
                  // defined from the express happiness framework
                  var msg = '-- WARNING -- There is currently no control method defined for route ' + path.join('/') + '/' + nodeName + '. Please create it on /controllers/routesControllerFunctions.js and assign it to functions["' + path.join('/') + '/' + nodeName + '"]';
                    console.log(msg.red.bgWhite);
                    theRoute[supportedTypes[k]](noControlMethodCallback);
                }
            }


            console.log('Route ' + supportedTypes[k].toUpperCase() + ' "'  + path.join('/') + '/' + nodeName + '" created successfully'.green);
        }
    }
    // if the node has a subroutes property recursively call this function
    // for each subroute 
    if(node.hasOwnProperty('subRoutes')){
        var newPath = path.slice(0);
        newPath.push(nodeName);
        for(var property in node.subRoutes) {
            if(node.subRoutes.hasOwnProperty(property)){ // first we check that the property is not an inherited one
                generateNodeRoutes(node.subRoutes[property], property, newPath, router, baseUrl, preValidationMiddlewares, postValidationMiddlewares, inheritedGroups);
            }
        }
    }
    
    // use the current iterations router
    app.use('', router);

    // register a generic catch all middleware
    app.get('*', function(req, res, next){
        var err = new Error();
        err.type = '404';
        return next(err);
    });
}


/**
 * Augments the request object with an expresshappiness object
 * containing the apiMethod {one of get, post, put, delete}
 * @param  {string}   method The httpmethod
 * @return {Function}        Express middleware function
 */
var putMethodsOnReq = function(method){
    return function(req, res, next){
        req.expressHappiness = {};
        req.expressHappiness.apiMethod = method;
        req.expressHappiness.get = getGetter(req);
        req.expressHappiness.set = getSetter(req);
        return next();
    }
}

/**
 * Creates and returns a function with the mocking data defined if
 * the mocking operation is enabled and the mocking file for the current path is present
 * @param  {string} type     The http method for the request
 * @param  {[type]} path     The array containing all the parts of the current path
 * @param  {[type]} nodeName The name of the endpoint
 * @param  {[type]} alias    The node's alias as defined in the routes file
 * @return {Function}        A middleware function with just a call to next if no mocking
 * is present or a function that creates a mock query function that returns the mocked data
 */
var mockQueryGenerationMiddleWare = function(type, path, nodeName, alias){
    if(!global.expressHappiness.confObj.mockData.enable){
        return function(req, res, next){
            return next();
        }
    }

    var filename = '';
    var route = '';
    for(var i=1; i<path.length; i++){
        filename += path[i] + '.';
        route += path[i] + '/';
    }
    route += nodeName;
    filename += nodeName;

    if(alias){
        if(fs.existsSync(global.expressHappiness.confObj.mockData.folder + '/' + alias + '.json')){
            var mockData = require(global.expressHappiness.confObj.mockData.folder + '/' + alias + '.json');

            return function(req, res, next){
                req.expressHappiness.mockQuery = function(success){
                    success(mockData);
                }
                return next();
            }
        }
    }


    if(fs.existsSync(global.expressHappiness.confObj.mockData.folder + '/' + type + '.' + filename + '.json')){
        var mockData = require(global.expressHappiness.confObj.mockData.folder + '/' + type + '.' + filename + '.json');

        return function(req, res, next){
            req.expressHappiness.mockQuery = function(success){
                success(mockData);
            }
            return next();
        }
    } else {
        var msg = '-- WARNING -- No mock data found for ' + route + ' route. Please create file named ' + filename + '.json and place it on mockJSONs folder';
        console.log(msg.red.bgWhite);
        return function(req, res, next){
            req.expressHappiness.mockQuery = function(success){
                var err = new Error();
                err.type = 'noMockData';
                return next(err);
            };
            return next();
        }
    }
}

// getter method for fetching the query of body field based on the method
var getGetter = function(req){
    if(req.expressHappiness.apiMethod == 'get'){
        return function(key){
            return req.query[key];;
        }
    } else {
        return function (key) {
            return req.body[key];
        }
    }
}

// setter method for fetching the query of body field based on the method
var getSetter = function(req){
    if(req.expressHappiness.apiMethod == 'get'){
        return function(key, value){
            req.query[key] = value;
        }
    } else {
        return function(key, value){
            req.body[key] = value;
        }
    }
}

// default controller method middleware
var noControlMethodCallback = function(req, res, next){
    var err = new Error("Under Development");
    err.type = 'underDevelopment';
    return next(err);
}

/**
 * Augmets the req.expressHappiness object with 
 * the apiPath, the routeAlias, the mock function
 * @param  {[type]} path     [description]
 * @param  {[type]} nodeName [description]
 * @param  {[type]} alias    [description]
 * @param  {[type]} mock     [description]
 * @return {[type]}          [description]
 */
var putApipathToReq = function(path, nodeName, alias, mock){
    return function(req, res, next){
        var newPath = path.slice(0);
        newPath.shift();
        newPath.push(nodeName);
        req.expressHappiness.apipath = newPath;
        req.expressHappiness.routeAlias = alias;
        req.expressHappiness.mock = mock;
        return next();
    }
}

/**
 * Handles the last step of the mocking process
 * if the mocking functionality is enabled and the mock function
 * exists in the req.expressHappiness object it returns the result of calling the
 * function
 * @param  {Express Request}   req  
 * @param  {Express Response}   res  
 * @param  {Function} next 
 * @return {Function|undefined} 
 */
var mockMiddlewareApplied = function(req, res, next){
    if(global.expressHappiness.confObj.mockData.global){
        req.expressHappiness.mockQuery(function(results){
            return res.send(results);
        });
    } else if((req.expressHappiness.get('mock') != 1 && !req.expressHappiness.mock) || !global.expressHappiness.confObj.mockData.enable){
        return next();
    } else {
        req.expressHappiness.mockQuery(function(results){
            return res.send(results);
        });
    }
}

/**
 * Fetches the user defined errors, defines some default errors
 * and then registers all of them as a error middleware
 * @param  {Object} app                     The express instanse
 * @param  {string} errorsConfigurationFile 
 * @param  {string} errorFile               The error log filepath
 */
var registerErrors = function(app, errorsConfigurationFile, errorFile){

    // fetch the user defined errors
    var errors = require(errorsConfigurationFile).errors;

    // ExpressHappiness undefined error
    var undefinedError = {
        log:true,
        humanReadable: 'Unresolved error code',
        sendToClient: {
            code:500
        }
    };

    // ExpressHappiness invalid attributes error
    var invalidAttrs = {
        log:true,
        humanReadable: 'Invalid attributes passed',
        sendToClient: {
            code:400,
            data:'err.details'
        }
    };

    // ExpressHappiness 404 error
    var fourZeroFour = {
        log:false,
        humanReadable: 'The requested resource does not exist',
        sendToClient: {
            code:404
        }
    };

    // ExpressHappiness 404 error
    var noMockData = {
        log: true,
        humanReadable: 'There is no mock data available for this route yet',
        sendToClient: {
            code: 404,
            data:'There is no mock data available for this route yet'
        }
    };

    // ExpressHappiness under development error
    var underDevelopment = {
        log: false,
        humanReadable: 'A call to a route under development has been made',
        sendToClient:{
            code:501,
            data:'This route is currently under development'
        }
    };

    // extending the user defined errors with the express happiness defined errors
    if(!errors.hasOwnProperty('undefinedError')) {
        errors.undefinedError = undefinedError;
    } else {
        errors.undefinedError = formatError(undefinedError, errors.undefinedError)
    }
    if(!errors.hasOwnProperty('invalidAttrs')) {
        errors.invalidAttrs = invalidAttrs;
    } else {
        errors.invalidAttrs = formatError(invalidAttrs, errors.invalidAttrs)
    }
    if(!errors.hasOwnProperty('404')) {
        errors['404'] = fourZeroFour;
    } else {
        errors['404'] = formatError(fourZeroFour, errors['404'])
    }
    if(!errors.hasOwnProperty('noMockData')) {
        errors.noMockData = noMockData;
    } else {
        errors.noMockData = formatError(noMockData, errors.noMockData)
    }
    if(!errors.hasOwnProperty('underDevelopment')) {
        errors.underDevelopment = underDevelopment;
    } else {
        errors.underDevelopment = formatError(underDevelopment, errors.underDevelopment)
    }

    // require the error handler module and instansiate it
    var ErrorHandlerModule = require('./ErrorHandler.js');
    var ErrorHanlder = new ErrorHandlerModule(errorFile);

    // register a generic error middleware that returns an error finding it using the closure from above
    app.use(function (err, req, res, next) {
        if(errors.hasOwnProperty(err.type)){
            ErrorHanlder.handleError(errors[err.type], err, req, res);
        } else {
            ErrorHanlder.handleError(errors.undefinedError, err, req, res);
        }
    });
};

// formats the error passed to the client side
function formatError(error, options){
    var keys = Object.keys(options);
    for(var i=0; i<keys.length; i++){
        var key = keys[i];
        error[key] = options[key];
    }
    return error;
}